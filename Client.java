
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

public class Client {
    private Socket serverSocket = null;
    public static BufferedReader r;
    public InetAddress group;
    public static PrintStream os;
    public static DataInputStream is;
    public AudioFormat format;
    public TargetDataLine microphone;
    public SourceDataLine speakers;
    public MulticastSocket socket;
    public final int CHUNK_SIZE = 500;
    public static final int maxClients = 10;
    public String args;

    public Client(int port, String host)
            throws IOException, LineUnavailableException, InterruptedException {

        // set up audio formatting and datagram socket
        format = new AudioFormat(8000.0f, 16, 1, true, true);
        group = InetAddress.getByName("225.0.0.1");
        socket = new MulticastSocket(8001);
        socket.setLoopbackMode(true);
        socket.joinGroup(group);

        //connect to server
        serverSocket = new Socket(host, port);
        os = new PrintStream(serverSocket.getOutputStream());
        is = new DataInputStream(serverSocket.getInputStream());
        System.out.println("Enter Your name");
        os.println(r.readLine());

        TalkToServerThread tts = new TalkToServerThread();
        tts.start();
        ListenToServerThread lts = new ListenToServerThread();
        lts.start();
    }

    public static void main(String args[])
            throws IOException, LineUnavailableException, InterruptedException {
        int port;
        String tempPort;
        String host;
        r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter Server port number");
        while ((tempPort = r.readLine()).length() < 1) {
            System.out.println("Enter port number cunt");
        }
        port = Integer.parseInt(tempPort);
        System.out.println("Enter Server host");
        while ((host = r.readLine()).length() < 1) {
            System.out.println("Enter Server host cunt");
        }
        new Client(port, host);
    }

    class TalkToServerThread extends Thread {

        @Override
        public void run() {
            try {
                while (true) {
                    String name;
                    System.out.println("Enter option");
                    String option = r.readLine();
                    switch (option) {
                        case "$talk$":
                            os.println(option);
                            System.out.println("Enter the name of who you would like to talk to ");
                            os.println(r.readLine());
                            break;
                        case "$group$":
                            os.println(option);
                            System.out.println("Enter each name. Enter # to end listing");
                            while (!(name = r.readLine()).equals("#")) {
                                os.println(name);
                            }
                            os.println(name);
                            break;
                        case "$msg$":
                            os.println(option);
                            System.out.println("enter <name>: and the message");
                            String in = r.readLine();
                            int col = (in).indexOf(':');
                            name = in.substring(0, col);
                            String message = in.substring(col);
                            os.println(name);
                            os.println(message);
                            break;
                        case "$end$":
                            os.println(option);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class ListenToServerThread extends Thread {

        @Override
        public void run() {
            ListenThread lt = null;
            TalkThread tt = null;
            try {
                while (true) {
                    String input = is.readLine();
                    switch (input) {
                        case "-listen-":
                            (lt = new ListenThread()).start();
                            break;
                        case "-talk-":
                            (tt = new TalkThread()).start();
                            break;
                        case "-msg-":
                            System.out.println(is.readLine());
                            break;
                        case "-end-":
                            tt.end();
                            lt.end();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        }
    }

    class TalkThread extends Thread {
        DatagramPacket packet;
        public boolean talking = true;

        public TalkThread() throws LineUnavailableException, IOException {
            microphone = AudioSystem.getTargetDataLine(format);
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            microphone = (TargetDataLine) AudioSystem.getLine(info);
            microphone.open(format);
            microphone.start();
        }

        @Override
        public void run() {
            System.out.println("Talking...");
            int numBytesRead;
            byte[] data = new byte[microphone.getBufferSize() / 5];
            while (talking) {
                numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
                try {
                    if (calculateRMSLevel(data) > 30) {
                        packet = new DatagramPacket(data, numBytesRead, group, 8001);
                        socket.send(packet);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Talking ended");
        }

        public int calculateRMSLevel(byte[] audioData)
        {
            long lSum = 0;
            for(int i=0; i < audioData.length; i++)
                lSum = lSum + audioData[i];

            double dAvg = lSum / audioData.length;
            double sumMeanSquare = 0d;

            for(int j=0; j < audioData.length; j++)
                sumMeanSquare += Math.pow(audioData[j] - dAvg, 2d);

            double averageMeanSquare = sumMeanSquare / audioData.length;

            return (int)(Math.pow(averageMeanSquare,0.5d) + 0.5);
        }

        public void end(){
            this.talking = false;
        }
    }

    class ListenThread extends Thread {
        public boolean listening = true;

        public ListenThread() throws IOException, LineUnavailableException {

            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
            speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            speakers.open(format);
            speakers.start();
        }

        @Override
        public void run() {
            System.out.println("Listening...");
            DatagramPacket packet;
            byte buf[];
            while (listening) {
                buf = new byte[CHUNK_SIZE];
                packet = new DatagramPacket(buf, CHUNK_SIZE);
                try {
                    socket.receive(packet);
                    speakers.write(buf, 0, CHUNK_SIZE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("listening ended");
        }

        public void end(){
            this.listening = false;
        }
    }

}
