import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static ServerSocket serverSocket = null;
    private static Socket clientSocket = null;
    public static final int maxClients = 10;
    public ClientThread[] ct;
    private boolean end = false;

    public static void main(String args[]) throws IOException {

        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter port number");
        String port;
        while ((port = r.readLine()).length() < 1) {
            System.out.println("Enter port number cunt");
        }
        Server s = new Server(Integer.parseInt(port));
    }

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        System.out.println("Sitting here being a server");
        ct = new ClientThread[maxClients];

        while (true) {
            clientSocket = serverSocket.accept();
            for (int i = 0; i < maxClients; i++) {
                if (ct[i] == null) {
                    ct[i] = new ClientThread(clientSocket);
                    break;
                }
            }
        }
    }

    class ClientThread extends Thread {
        public String name;
        public Socket mySocket;
        public PrintStream os;
        public DataInputStream is;

        public ClientThread(Socket mySocket) throws IOException {
            this.mySocket = mySocket;
            os = new PrintStream(mySocket.getOutputStream());
            is = new DataInputStream(mySocket.getInputStream());
            System.out.println((name = is.readLine()) + " is now connected");
            this.start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    String name;
                    String names[] = new String[maxClients - 1];
                    String option = is.readLine().trim();
                    System.out.println("running... " + option);
                    switch (option) {
                        case "$talk$":
                            os.println("-listen-");
                            chat(is.readLine().trim());
                            os.println("-talk-");
                            break;
                        case "$group$":
                            int i = 0;
                            while (!(name = is.readLine().trim()).equals("#")) {
                                names[i] = name;
                                i++;
                            }
                            os.println("-listen-");
                            groupChat(names, i);
                            os.println("-talk-");
                            break;
                        case "$end$":
                            os.println("-end-");
                            break;
                        case "$msg$":
                            names[0] = is.readLine();
                            String message = is.readLine();
                            message(names, message, 1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void chat(String name) {
            System.out.println("chat running");
            for (int i = 0; i < maxClients; i++) {
                if (ct[i] != null && (ct[i].name).equals(name)) {
                    ct[i].os.println("-listen-");
                    ct[i].os.println("-talk-");
                }
            }
        }

        public void groupChat(String names[], int n) {
            System.out.println("group chat running");
            //send the hosts to this client
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < maxClients; j++) {
                    if (ct[j] != null && (ct[j].name).equals(names[i])) {
                        ct[j].os.println("-listen-");
                        ct[j].os.println("-talk-");
                    }
                }
            }

        }

        public void message(String names[], String message, int n){
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < maxClients; j++) {
                    if (ct[j] != null && (ct[j].name).equals(names[i])) {
                        ct[j].os.println("-msg-");
                        ct[j].os.println(this.name +  message);
                    }
                }
            }
        }
    }
}
